import { reduce, add } from 'lodash/fp';
import { sum, multiply } from './ops';

console.log(multiply(sum(2, 3), 4));
console.log(reduce(add, 0, [1, 2, 3]));
