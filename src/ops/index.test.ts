import { multiply, sum, subtract, divide } from '.';

test.each([sum, multiply, subtract, divide])('%p should be defined', (func) => {
  expect(func).toBeDefined();
});
