import sum from './sum';
import multiply from './multiply';
import subtract from './subtract';
import divide from './divide';

export { sum, multiply, subtract, divide };
