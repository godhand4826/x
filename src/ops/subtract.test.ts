import subtract from './subtract';

test.each([
  [1, 1, 0],
  [1, 3, -2],
  [0.3, 2, -1.7],
])('subtract(%s, %s) should equal to %s', (a, b, expected) => {
  expect(subtract(a, b)).toBe(expected);
});
