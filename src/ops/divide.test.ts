import divide from './divide';

test.each([
  [10, 2, 5],
  [6, 2, 3],
  [0, 4, 0],
  [5, 1, 5],
  [5, 2, 2.5],
  [1, 3, 0.3333333333333333],
])('divide(%s, %s) should equal to %s', (a, b, expected) => {
  expect(divide(a, b)).toEqual(expected);
});

test.each([0, 1, 2])('divide(%s, 0) should throw', (a) => {
  expect(() => divide(a, 0)).toThrowError(
    new Error('could not divide by zero'),
  );
});
