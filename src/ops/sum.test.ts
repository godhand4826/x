import sum from './sum';

test.each([
  [1, 0, 1],
  [0, 1, 1],
  [1, 1, 2],
  [1, 2, 3],
  [2, 1, 3],
  [-1, 2, 1],
  [1, -0.2, 0.8],
])('sum(%s, %s) should equal to %s', (a, b, expected) => {
  expect(sum(a, b)).toBe(expected);
});
