import multiply from './multiply';

test.each([
  [2, 3, 6],
  [1, 9, 9],
  [8, 0, 0],
  [-2, 3, -6],
])('multiply(%s, %s) should equal to %s', () => {
  expect(multiply(2, 3)).toBe(6);
});
