FROM node:16-alpine as package_config
WORKDIR /app
COPY package.json package-lock.json ./

FROM package_config as prod_dependencies
RUN npm ci --production

FROM prod_dependencies as dependencies
RUN npm ci

FROM dependencies as builder
COPY . .
RUN npm run format && npm run lint && npm run test && npm run build

FROM node:16-alpine AS release
USER node:node
WORKDIR /app
COPY --chown=node:node --from=prod_dependencies /app/node_modules ./node_modules
COPY --chown=node:node --from=builder /app/dist .
CMD node index.js