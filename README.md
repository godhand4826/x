# x

[![pipeline status](https://gitlab.com/godhand4826/x/badges/master/pipeline.svg)](https://gitlab.com/godhand4826/x/-/commits/master) [![coverage report](https://gitlab.com/godhand4826/x/badges/master/coverage.svg)](https://gitlab.com/godhand4826/x/-/commits/master)

## Description

A practice of gitlab ci with typescript, prettier, eslint, jest, docker, etc.

## Installation

```bash
$ npm ci
```

## Running the app

```bash
# development mode
$ npm run start:dev

# production mode
$ npm run build
$ npm run start
```

## Formater and Linter

```bash
# check format
$ npm run format
# fix format
$ npm run format:fix
# check style
$ npm run lint
# fix style
$ npm run lint:fix
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:coverage
```
